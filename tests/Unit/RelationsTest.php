<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Task;
use App\User;

class RelationsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTask()
    {
        $task = factory(Task::class)->make();
        $user_id = User::find($task->user_id);
        $this->assertEquals($task->user->id, $user_id->id);
    }
}